﻿using Android;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;

namespace RestartService
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            FindViewById<Button>(Resource.Id.start).Click += delegate
            {
                StartService(new Intent(this, typeof(App1.RestartService)));
            };

            FindViewById<Button>(Resource.Id.stop).Click += delegate
            {
                StopService(new Intent(this, typeof(App1.RestartService)));
            };
        }
    }
}

