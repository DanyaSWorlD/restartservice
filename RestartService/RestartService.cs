﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using System.Threading;
using Android;
using Android.Util;
using Android.Graphics;
using Android.Support.V4.App;

namespace App1
{
    [Service]
    public class RestartService : Service
    {
        static readonly string TAG = "X:" + typeof(RestartService).Name;
        static readonly int TimerWait = 4000;
        Timer _timer;
        PowerManager powerManager;
        PowerManager.WakeLock wakeLock;
        internal static bool recieved;
        private bool running;

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (running) return StartCommandResult.RedeliverIntent;
            running = true;
            try
            {
                Log.Debug(TAG, "OnStartCommand called at {2}, flags={0}, startid={1}", flags, startId, DateTime.UtcNow);
                _timer = new Timer(o =>
                {
                    Log.Debug(TAG, "Hello from restart service {0}", DateTime.UtcNow);
                    Intent i = new Intent();
                    i.SetAction("hello");
                    recieved = false;
                    SendBroadcast(i);
                    Thread.Sleep(1000);
                    if (!recieved)
                    {
                        Intent dialogIntent = PackageManager.GetLaunchIntentForPackage("com.daquga.FailingApp");
                        if (dialogIntent != null) //null pointer check in case package name was not found
                        {
                            dialogIntent.AddFlags(ActivityFlags.NewTask);
                            StartActivity(dialogIntent);
                        }
                    }
                }, null, 0, TimerWait);

                var channelName = "RestartSeviceNotificationChannel";
                using (var notificationManager = NotificationManager.FromContext(ApplicationContext))
                {
                    if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
                    {
                        NotificationChannel channel = null;
#if !DEBUG
                            channel = notificationManager.GetNotificationChannel(channelName);
#endif
                        if (channel == null)
                        {
                            channel = new NotificationChannel(channelName, channelName, NotificationImportance.Low)
                            {
                                LockscreenVisibility = NotificationVisibility.Public
                            };
                            channel.SetShowBadge(true);
                            notificationManager.CreateNotificationChannel(channel);
                        }
                        channel.Dispose();
                    }
                    Bitmap bitMap = BitmapFactory.DecodeResource(Resources, Resource.Drawable.IconA);
                    var notificationBuilder = new NotificationCompat.Builder(ApplicationContext)
                        .SetContentTitle("App Service")
                        .SetContentText("App Running!")
                        .SetSmallIcon(Resource.Drawable.IconA)
                        .SetLargeIcon(bitMap)
                        .SetShowWhen(false)
                        .SetChannelId(channelName);

                    Notification note = notificationBuilder.Build();
                    StartForeground((int)NotificationFlags.ForegroundService, note);
                    return StartCommandResult.RedeliverIntent;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка в функции OnStartCommand: {0}", ex.Message);
                StopService(new Intent(this, typeof(RestartService)));
                return StartCommandResult.RedeliverIntent;
            }
        }

        public override void OnTaskRemoved(Intent rootIntent)
        {
            try
            {
                Intent restartService = new Intent(ApplicationContext, this.Class);
                restartService.SetPackage(PackageName);
                PendingIntent restartServicePI = PendingIntent.GetService(ApplicationContext, 1, restartService, PendingIntentFlags.OneShot);
                //Restart the service once it has been killed android
                AlarmManager alarmService = (AlarmManager)ApplicationContext.GetSystemService(AlarmService);
                alarmService.Set(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + 100, restartServicePI);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Write Error on File as Log
            }
        }


        public override void OnCreate()
        {
            base.OnCreate();
            var filter = new IntentFilter("helloAnsw");
            RegisterReceiver(new MReciever(), filter);
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnDestroy()
        {
            try
            {
                base.OnDestroy();

                _timer.Dispose();
                _timer = null;

                if (TAG != null)
                {
                    Log.Debug(TAG, "RestartService destroyed at {0}.", DateTime.UtcNow);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Write Error on File as Log
            }
        }

        private class MReciever : BroadcastReceiver
        {
            public override void OnReceive(Context context, Intent intent)
            {
                if (intent.Action != "helloAnsw")
                    return;
                Log.Debug("X:", "answer recieved");
                recieved = true;
            }
        }
    }
}