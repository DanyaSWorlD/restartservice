﻿
 using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Content;
using Android.Util;

namespace App1
{
	[Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
	public class MainActivity : AppCompatActivity
	{

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.activity_main);

			Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FindViewById<Button>(Resource.Id.button1).Click += delegate
            {
                while(true)
                {
                    //Chill
                }
            };

            FindViewById<Button>(Resource.Id.button2).Click += delegate
            {
                Finish();
            };

            FindViewById<Button>(Resource.Id.button3).Click += delegate
            {
                var arr = new int[0];
                var a = arr[10];
            };

            FindViewById<Button>(Resource.Id.button4).Click += delegate
            {
                throw new ArgumentOutOfRangeException("button", "", "button cant be more than 3");
            };

            var filter = new IntentFilter("hello");
            RegisterReceiver(new MReciever(), filter);
		}

		public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        private class MReciever : BroadcastReceiver
        {
            public override void OnReceive(Context context, Intent intent)
            {
                if (intent.Action == "hello")
                {
                    Log.Debug("X:", "Hello intent resieved");
                    var i = new Intent();
                    i.SetAction("helloAnsw");
                    new ContextWrapper(context).SendBroadcast(i);
                }
            }
        }
	}
}

